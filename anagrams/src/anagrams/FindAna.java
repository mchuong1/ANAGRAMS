package anagrams;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mstch
 * The purpose of the FindAna is for it to hold my methods that
 * solve the problem instead of anagrams holding it. Plus, it makes both
 * classes look neater and easier to edit.
 */
 

import java.io.*;
import java.util.*;

public class FindAna {
    
    
    public static void Sig(String[] a) {
        //This method will turn all words into lowercase so that it is easier for comparing
        //Also it will sort the characters in the word alphabetically
        //Pre: Text File
        //Post: A String array containing all the signatures
        
        
        for(int i = 0; i < a.length; i++)
        {
            if(a[i].length() > 15)//skips if word has more than 15 characters
            {
                continue;
            }
                
            a[i] = a[i].replaceAll("\\p{Punct}+", "");//removes all punctuation        
            a[i] = a[i].replaceAll("\\d",""); //removes numbers
            a[i] = a[i].toLowerCase();
            
            char[] chars = a[i].toCharArray();
            Arrays.sort(chars);
            String result = new String(chars);            
            a[i] = result;
            
        }   
    }
    
    public static void noB(String[] a)
    {
        //Removes extra brackets from the words
        //pre: txt file
        //post: removes unnecessary punctuation from original document
        for(int i = 0; i < a.length; i++)
        {
            
            a[i] = a[i].replace(",", "");
            a[i] = a[i].replace("[", "");
            a[i] = a[i].replace("]", "");
            
        }
    }
    
    public static void Compare(String[] a, String[] b)
    {
        //sort the signatures and original next to each other        
        //Pre: The original list and the Signature list
        //Post: anagrams will be right next to each other
        
        for(int i = 0; i < a.length - 1; i++)//sets the condition
        {
            for(int j = 1; j < a.length; j++)//checks through the whole array
            {
                if(a[i].equals(a[j]))
                {
                    if(i == j)//dpes nothing if its on the same position
                    {
                    continue;    
                    }
                    String temp = a[i + 1];
                    a[i +1] = a[j];
                    a[j] = temp;
                    
                    temp = b[i + 1];
                    b[i +1] = b[j];
                    b[j] = temp;
                }
            }
        }   
    }
    
    public static void FinalPrint(String[] a, String[] b)
    {
        //C:\Users\mstch\OneDrive\Documents\UserInput.txt
        //print all the anagrams found in the list
        //Pre:The sorted original txt and signature list
        //Post:print out all the anagrams on the same line onto Output.txt
        try
        {
            FileWriter fw = new FileWriter("Output.txt");
            PrintWriter pw = new PrintWriter(fw);
            
            int i = 0;
            for(i = 0; i < a.length - 1; i++)
            {
                if(!a[i].matches(a[i + 1]))
                {
                    pw.println(b[i]);
                    System.out.println(b[i]);
                    continue;
                }
                pw.print(b[i] + " ");
                System.out.print(b[i] + " ");
            }
        pw.println(b[i]);
        System.out.println(b[i]);
        pw.close();
        }    
        catch(IOException e)
        {
                        System.out.println(e);
        }
    }
        
    
    
        public static void Print(String[] a)//prints arrays used for checking
    {
            //prints arrays
            //Pre: has to be String array
            //Post: prints the array 
        for(int i = 0; i < a.length; i++)
            {
               System.out.print(a[i] + " ");
            }        
        System.out.println();
    }
}

