/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anagrams;

/**
 *
 * @author Matthew Chuong
 * 
 * This program finds anagrams from a file given from the user
 * and it will print the anagrams next to each other.
 * It will ignore 15 letter long or longer words
 * It wont compute if it has more than 50 words
 * It will ignore numbers
 * Expected Input: A file that is not larger than 50 and no numbers
 * Expected output: To print the anagrams next to each other
 * and put them on separate lines from other anagrams
 * 
 * Anagram Class will ask for the file and execute methods
 * from FindAna
 * I have developed 5 methods to solve the problem at hand
 * Sig, Compare, Print, FinalPrint, and noB
 * I also use String array data structures to solve the problem
 * The vector and scanner class also helped achieve the solution
 * 
 * The purpose of the FindAna is for it to hold my methods that
 * solve the problem instead of anagrams holding it. Plus, it makes both
 * classes look neater and easier to edit.
 */

import java.io.*;
import java.util.*;

public class Anagrams {

    /**
     * @param args the command line arguments
     * 
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in);
        System.out.println("Enter file: ");
        
        File f = new File(in.nextLine());//Enter user txt
        
        FindAna ana = new FindAna();//so i can use my methods from the Findana class       
        Vector v = new Vector();//keeper of the words       
        try
        {
            
            in = new Scanner(f);//scanner will now read the file
            int counter = 0;//use split method
            
            while(in.hasNextLine())        
            {
                //to see if there are other lines in the file
                //puts the words into arrays
                String line = in.nextLine();                        
                
                String[] og = line.split(" ");//seperate words
                for(int i = 0; i < og.length; i++)//puts them in vector one by one
                {
                    if(og[i].length() > 15 || og[i].isEmpty())
                    {
                        continue;
                    }
                    v.add(og[i]);
                    counter ++;//counts how many words are in the txt file
                }
                
            }
            if(counter > 50 || counter == 0)//terminates if txt file has more than 50 words or if it is empty
            {
                System.out.println("This txt file has more than 50 words or its empty.");
                System.exit(0);
            }
            String temp = v.toString();//turns vector to string
            String[] list = temp.split(" ");//seperate again and original list
            String[] copy = temp.split(" ");//editable version            
            ana.Sig(copy);//this is the signature list
            ana.noB(list);//removes unnecessarry brackets and commas from original list
            ana.Compare(copy,list);//sort out list so that anagrams
            
            System.out.println("Anagrams");
            ana.FinalPrint(copy,list);  
      
      in.close();
    }
    
        catch(FileNotFoundException e)//need this because you call a file earlier but it cannot find it (bc user inputs it)
        {   
            System.out.println("The input file is empty");                        
        }
    }    
}

